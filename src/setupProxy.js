const proxy = require("http-proxy-middleware");

module.exports = function(app){
    app.use(
        proxy("/rawblock/0000000000000bae09a7a393a8acded75aa67e46cb81f7acaa5ad94f9eacd103", {
            target:"https://blockchain.info",
            secure:false,
            changeOrigin: true
        })
    )

    app.use(
        proxy("/latestblock", {
            target:"https://blockchain.info",
            secure:false,
            changeOrigin: true
        })
    )
}