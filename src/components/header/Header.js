import React from 'react';

import './header.css'

function Header(){
    return(
        <header className="header">
            {/* Header with logo nothing else. */}
            <div className="logo">
                <img src="./chainyard-logo.svg" alt="Chainyard logo" />
            </div>
        </header>
    )
}
export default Header;