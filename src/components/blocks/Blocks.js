import React, { Component } from 'react';
import Blocklist from './Blocklist';
import TransBlock from './TransBlock';
import './blocks.css';

class Blocks extends Component{
    render(props){
        const block = this.props.data;
        const latest = this.props.latest;
        return(
            <section className="section">
                <div className="container">

                    {/* Blocks */}
                    <div className="flexx flex-wrap jc-sb" style={{display: this.props.transaction ? 'none' : ''}}>

                        {/* Latest Block */}
                        <div className="blocks">
                            <h2 className="blocks__title">Latest Block</h2>
                            <Blocklist {...this.props} list={latest} />
                        </div>

                        {/* Blocks */}
                        <div className="blocks">
                            <h2 className="blocks__title">Block</h2>
                            <Blocklist {...this.props} list={block} />
                        </div>
                    </div>

                    {/* Transaction list block */}
                    <div className="transBlock" style={{display: this.props.transaction ? '' : 'none'}}>
                        <h2 className="blocks__title"><button className="link link-back" onClick={this.props.showBlock.bind(this)}>Back to Block</button>Transaction Block</h2>
                        <TransBlock {...this.props} />
                    </div>
                </div>
            </section>
        )
    }
}
export default Blocks;