import React, { Component } from 'react';

class TransBlock extends Component{
    render(props){
        const block = this.props.block;
        //console.log(block);
        return(
            <div className="flexx flex-wrap jc-sb">
                {block.tx 
                    ?
                        block.tx.map((trans) => {
                            return(
                                <div key={trans.hash} className="blocks">
                                    {/* All transaction repeat */}
                                    <ul className="blocks__list">
                                        <li><strong>hash</strong><span className={`seeHash`} onClick={this.props.isExpand.bind(this)} title={trans.hash}>{trans.hash}</span></li>
                                        <li><strong>Weight</strong> <span>{trans.weight}</span></li>
                                        <li><strong>Time</strong> <span>{trans.time}</span></li>
                                        <li><strong>fee</strong> <span>{trans.fee}</span></li>
                                        <li><strong>size</strong> <span>{trans.size}</span></li>
                                        <li><strong>Relayed</strong> <span>{trans.relayed_by}</span></li>
                                    </ul>

                                    {/* Transactions (Transactions, input and out with condition if defined then only it will show.) */}
                                    {trans.inputs 
                                        ? 
                                        <div>
                                            {/* Input */}
                                            <h3>Input</h3> 
                                            <div className="flexx flex-wrap jc-sb">
                                            {trans.inputs.map((list,index) => {
                                                return (
                                                    <div key={index} className="block__input">
                                                        <ul className="blocks__list">
                                                        <li><strong>Sequence</strong> <span>{list.sequence}</span></li>
                                                        <li><strong>script</strong> <span className="seeHash" onClick={this.props.isExpand.bind(this)} title={list.script}>{list.script}</span></li>
                                                        </ul>
                                                    </div>
                                                )
                                            })}
                                            </div>

                                            {/* Output */}
                                            <h3>Output</h3> 
                                            <div className="flexx flex-wrap jc-sb">
                                            {trans.out.map((list,index) => {
                                                return (
                                                    <div key={index} className="block__input">
                                                        <ul className="blocks__list">
                                                        <li><strong>spent</strong> <span>{list.spent ? 'Yes' :'No'}</span></li>
                                                        <li><strong>value</strong> <span>{list.value}</span></li>
                                                        <li><strong>script</strong> <span className="seeHash" onClick={this.props.isExpand.bind(this)} title={list.script}>{list.script}</span></li>
                                                        </ul>
                                                    </div>
                                                )
                                            })}
                                            </div>
                                        </div>
                                        : 
                                        ''}
                                    

                                </div>
                            )
                        })
                    :
                        ''
                }
            </div>
        )
    }
}
export default TransBlock;