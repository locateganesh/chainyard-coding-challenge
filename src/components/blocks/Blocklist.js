import React, { Component } from 'react';

import './blocks.css';


class Blocklist extends Component{
    
    render(props){
        const list = this.props.list;
        return(
            <ul className="blocks__list">
                <li style={{display: list.hash ? '':'none'}}><strong>hash</strong> <span className={`seeHash`} onClick={this.props.isExpand.bind(this)} title={list.hash}>{list.hash}</span></li>
                <li style={{display: list.height ? '':'none'}}><strong>Height</strong> <span>{list.height}</span></li>
                <li style={{display: list.weight ? '':'none'}}><strong>Weight</strong> <span>{list.weight}</span></li>
                <li style={{display: list.time ? '':'none'}}><strong>Time</strong> <span>{list.time}</span></li>
                <li style={{display: list.bits ? '':'none'}}><strong>bits</strong> <span>{list.bits}</span></li>
                <li style={{display: list.fee ? '':'none'}}><strong>fee</strong> <span>{list.fee}</span></li>
                <li style={{display: list.size ? '':'none'}}><strong>size</strong> <span>{list.size}</span></li>
                <li style={{display: list.tx ? '':'none'}}><strong>Total Transactions</strong> <span>{list.tx ? list.tx.length : ''} <button className="link" onClick={this.props.showTrans.bind(this)}>See All</button></span></li>
            </ul>
        )
    }
}

export default Blocklist;