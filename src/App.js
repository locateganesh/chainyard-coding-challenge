import React, { Component } from 'react';
import axios from 'axios';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Blocks from './components/blocks/Blocks';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      block: [],
      latest:[],
      transaction: false
    }
  }

  // An ideal place to call API data.
  componentDidMount = () => {
    const blockHash = '/rawblock/0000000000000bae09a7a393a8acded75aa67e46cb81f7acaa5ad94f9eacd103';
    const latest = '/latestblock';
    
    axios.get(blockHash) // The CORS issue.
      .then(res => {
        // Setting state for blocks.
        const blockData = res.data;
        this.setState({ 
          block: blockData 
        });
    });
    

    axios.get(latest) 
    .then(res => {
      // Setting state for latest.
      this.setState({ 
        latest: res.data 
      });
    });

  }

  // click function to toggle to show long length Text.
  isExpand(e){
    return e.target.classList.toggle('expand');
  }

  // Show Transactions
  showTrans(){
    return this.setState({transaction: true});
  }
  // Hide Transactions
  showBlock(){
    return this.setState({transaction: false});
  }

  render(){
    return (
      <div className="App">
        {/* Header Component */}
        <Header />

        {/* Block Component includes all block and transaction blocks. */}
        <Blocks 
          {...this.state}
          data={this.state.block} 
          latest={this.state.latest}
          isExpand={this.isExpand.bind(this)}
          showTrans={this.showTrans.bind(this)}
          showBlock={this.showBlock.bind(this)}
        />

        {/* Footer Component */}
        <Footer />
      </div>
    );
  }
  
}

export default App;
